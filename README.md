# Bitcoin Core

A Bitcoin Core docker image.

## Tags

- `22.0`
- `0.21.1`
- `0.19.1`

## What is Bitcoin Core?

Bitcoin Core is a reference client that implements the Bitcoin protocol for remote procedure call (RPC) use. It is also the second Bitcoin client in the network's history. Learn more about Bitcoin Core on the [Bitcoin Developer Reference docs](https://bitcoin.org/en/developer-reference).

## Usage

### How to use this image


You must run a container with predefined environment variables.

Before setting up remote authentication, you will need to generate the rpcauth line that will hold the credentials for the Bitcoind Core daemon. You can either do this yourself by constructing the line with the format `<user>:<salt>$<hash>` or use the official [rpcauth.py](https://github.com/bitcoin/bitcoin/blob/master/share/rpcauth/rpcauth.py) script to generate this line for you, including a random password that is printed to the console.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_AUTH`                      | Auth to secure the JSON-RPC api.                                           |
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |

Run container: 

```bash
docker-compose up -d 

```

Stop container: 

```bash
docker-compose down 

```
